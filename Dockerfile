# Dockerfile for Elasticsearch

# Expose port 9200 and 9300(for cluster setups)

FROM nwlucas/ubuntu
MAINTAINER Nigel Williams-Lucas

ENV ES_VERSION 1.7.1
ENV PORT_TCP 9200
ENV PORT_CLUSTER 9300
ENV ES_HOME /usr/share/elasticsearch
ENV ES_CONF /usr/share/elasticsearch/config
ENV ES_USER elasticsearch
ENV ES_GROUP elasticsearch
ENV ES_DATA /usr/share/elasticsearch/data

LABEL Image.name="Elasticsearch" Image.version="${ES_VERSION}"
LABEL Exposed.ports.tcp="${PORT_TCP}" Exposed.ports.cluster="${PORT_CLUSTER}"
LABEL Base.OS="Ubuntu" Base.OS.Version="${UBUNTU_VERSION}"

# Elasticsearch installation
# Start Elasticsearch by /elasticsearch/bin/elasticsearch. This will run on port 9200.
RUN curl --progress-bar --remote-name --insecure https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-$ES_VERSION.tar.gz && \
   tar xf elasticsearch-$ES_VERSION.tar.gz && \
   rm elasticsearch-$ES_VERSION.tar.gz && \
   mv elasticsearch-$ES_VERSION ${ES_HOME} && \
   groupadd -r ${ES_GROUP} && useradd -r -g ${ES_GROUP} ${ES_USER}

ENV PATH ${ES_HOME}/bin:$PATH
COPY config ${ES_CONF}

RUN chown -R ${ES_USER}:${ES_GROUP} ${ES_HOME}/ ${ES_CONF} && \
    ${ES_HOME}/bin/plugin -install mobz/elasticsearch-head
VOLUME ${ES_DATA}

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE $PORT_TCP $PORT_CLUSTER

CMD ["elasticsearch"]